# 解决全屏游戏切回桌面软件合集

#### 介绍
解决玩全屏游戏时突遇最小化、弹出桌面。

1.  whoareyou.exe作者是一位NGA论坛用户。
2.  窗口活动监视工具作者是死性不改论坛用户维护大师技术A。链接：https://www.clxp.net.cn/thread-16584-1-1.html
3.  检测前台窗口作者是死性不改论坛用户Bluefish。链接：https://www.clxp.net.cn/thread-2272-1-1.html

#### 恢复
找到并双击再确认"恢复默认ForegroundLockTimeout.reg"文件即可恢复注册表项。

#### 使用火绒剑监控注册表方法

1.  安装火绒或更新火绒版本，官网：https://www.huorong.cn/
2.  打开火绒软件，点击安全工具
3.  在最下面找到火绒剑，下载并打开
4.  使用默认的系统选项卡不用动(左上角)
5.  点击过滤按钮，并选择路径过滤。点击添加按钮，类型由"路径"改为"注册表路径"，值输入"Control Panel\Desktop\ForegroundLockTimeout"，操作为"包含"
6.  点击开启监控
7.  祝你好运