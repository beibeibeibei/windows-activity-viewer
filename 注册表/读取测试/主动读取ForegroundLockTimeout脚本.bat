@echo off
setlocal enabledelayedexpansion

REM 读取注册表并打印读取到的值
reg query "HKEY_CURRENT_USER\Control Panel\Desktop" /v ForegroundLockTimeout > nul 2>&1
if %errorlevel% equ 0 (
    for /f "tokens=2*" %%a in ('reg query "HKEY_CURRENT_USER\Control Panel\Desktop" /v ForegroundLockTimeout ^| findstr /i "ForegroundLockTimeout"') do (
        echo %%b
    )
)

endlocal
